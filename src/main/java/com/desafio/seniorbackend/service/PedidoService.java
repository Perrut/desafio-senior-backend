package com.desafio.seniorbackend.service;

import java.util.UUID;

import org.springframework.data.domain.Page;

import com.desafio.seniorbackend.domain.entity.Pedido;

public interface PedidoService {
	public Pedido criarPedido(Pedido pedido);
	
	public Page<Pedido> listarPedidos(Integer page, Integer perPage);
	
	public Pedido atualizarPedido(UUID id, Pedido pedido);
	
	public Pedido obterPedido(UUID id);
	
	public void excluirPedido(UUID id);
}
