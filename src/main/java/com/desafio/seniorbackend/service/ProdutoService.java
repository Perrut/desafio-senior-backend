package com.desafio.seniorbackend.service;

import java.util.UUID;

import org.springframework.data.domain.Page;

import com.desafio.seniorbackend.domain.entity.Produto;

public interface ProdutoService {
	public Produto criarProduto(Produto produto);
	
	public Page<Produto> listarProdutos(Integer page, Integer perPage);
	
	public Produto atualizarProduto(UUID id, Produto produto);
	
	public Produto obterProduto(UUID id);
	
	public void excluirProduto(UUID id);
}
