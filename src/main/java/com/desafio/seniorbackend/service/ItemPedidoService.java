package com.desafio.seniorbackend.service;

import java.util.UUID;

import org.springframework.data.domain.Page;

import com.desafio.seniorbackend.domain.entity.ItemPedido;

public interface ItemPedidoService {

	public ItemPedido criarItemPedido(UUID pedidoId, ItemPedido itemPedido);

	public Page<ItemPedido> listarItensPedido(UUID pedidoId, Integer page, Integer perPage);

	public ItemPedido atualizarItemPedido(UUID pedidoId, UUID id, ItemPedido itemPedido);

	public ItemPedido obterItemPedido(UUID pedidoId, UUID id);

	public void excluirItemPedido(UUID pedidoId, UUID id);
	
	public ItemPedido itemPedidoComTotalCalculado(ItemPedido item);
}
