package com.desafio.seniorbackend.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.seniorbackend.domain.entity.ItemPedido;
import com.desafio.seniorbackend.domain.entity.Pedido;
import com.desafio.seniorbackend.domain.enums.SituacaoPedido;
import com.desafio.seniorbackend.domain.enums.TipoProduto;
import com.desafio.seniorbackend.domain.repository.PedidoRepository;
import com.desafio.seniorbackend.exception.PedidoFechadoException;
import com.desafio.seniorbackend.exception.PedidoNaoEncontradoException;
import com.desafio.seniorbackend.exception.ProdutoNaoEncontradoException;
import com.desafio.seniorbackend.service.ItemPedidoService;
import com.desafio.seniorbackend.service.PedidoService;

@Service
@Transactional
public class PedidoServiceImpl implements PedidoService {

	@Autowired
	private ItemPedidoService itemPedidoService;

	@Autowired
	private PedidoRepository pedidoRepository;

	@Override
	public Pedido criarPedido(Pedido pedido) {
		return pedidoRepository.save(pedido);
	}

	@Override
	public Page<Pedido> listarPedidos(Integer page, Integer perPage) {
		PageRequest pageRequest = PageRequest.of(page, perPage);
		Page<Pedido> queryPage = pedidoRepository.findAll(pageRequest);
		Page<Pedido> queryFormatada = queryPage.map(pedido -> pedidoComDescontoAplicado(pedidoComValorTotal(pedido)));
		return queryFormatada;
	}

	@Override
	public Pedido atualizarPedido(UUID id, Pedido pedido) {
		Pedido pedidoOriginal = pedidoRepository.findById(id).orElseThrow(() -> new PedidoNaoEncontradoException());

		pedidoOriginal.setDataCriacao(pedido.getDataCriacao());

		if (pedidoOriginal.getSituacaoPedido() == SituacaoPedido.FECHADO
				&& pedido.getSituacaoPedido() == SituacaoPedido.ABERTO) {
			throw new PedidoFechadoException("Pedido fechado não pode ser reaberto.");
		}
		pedidoOriginal.setSituacaoPedido(pedido.getSituacaoPedido());

		if (pedidoOriginal.getSituacaoPedido() == SituacaoPedido.ABERTO) {
			pedidoOriginal.setDesconto(pedido.getDesconto());
		} else if (pedido.getDesconto() > 0 && !pedido.getDesconto().equals(pedidoOriginal.getDesconto())) {
			throw new PedidoFechadoException("Pedido fechado, não podem ser mais aplicados descontos.");
		}

		pedidoOriginal = pedidoComDescontoAplicado(pedidoComValorTotal(pedidoOriginal));

		return pedidoRepository.save(pedidoOriginal);
	}

	@Override
	public Pedido obterPedido(UUID id) {
		Pedido pedido = pedidoRepository.findById(id).orElseThrow(() -> new PedidoNaoEncontradoException());

		return pedidoComDescontoAplicado(pedidoComValorTotal(pedido));
	}

	@Override
	public void excluirPedido(UUID id) {
		Pedido pedido = pedidoRepository.findById(id).orElseThrow(() -> new PedidoNaoEncontradoException());

		pedidoRepository.delete(pedido);
	}

	private Pedido pedidoComValorTotal(Pedido pedido) {
		Double valorTotal = 0.0;

		for (ItemPedido item : pedido.getItensPedido()) {
			valorTotal += itemPedidoService.itemPedidoComTotalCalculado(item).getTotal();
		}

		pedido.setValorTotal(valorTotal);

		return pedido;
	}

	private Pedido pedidoComDescontoAplicado(Pedido pedido) {
		if (pedido.getDesconto() > 0) {

			List<ItemPedido> produtosTipoProduto = pedido.getItensPedido().stream()
					.filter(item -> item.getProduto().getTipoProduto().equals(TipoProduto.PRODUTO))
					.collect(Collectors.toList());

			if (produtosTipoProduto.size() == 0) {
				throw new ProdutoNaoEncontradoException("Não há produtos tipo PRODUTO para aplicação do desconto.");
			}

			Double valorTotalProdutos = 0.0;

			for (ItemPedido item : produtosTipoProduto) {
				valorTotalProdutos += itemPedidoService.itemPedidoComTotalCalculado(item).getTotal();
			}

			if (valorTotalProdutos > 0) {
				Double valorDescontoPedido = valorTotalProdutos * (pedido.getDesconto() / 100);
				pedido.setValorComDesconto(pedido.getValorTotal() - valorDescontoPedido);
			}
		}

		return pedido;
	}
}
