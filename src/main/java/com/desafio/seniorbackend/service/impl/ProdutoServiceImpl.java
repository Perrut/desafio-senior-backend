package com.desafio.seniorbackend.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.seniorbackend.domain.entity.Produto;
import com.desafio.seniorbackend.domain.repository.ProdutoRepository;
import com.desafio.seniorbackend.exception.ProdutoEmPedidoException;
import com.desafio.seniorbackend.exception.ProdutoNaoEncontradoException;
import com.desafio.seniorbackend.service.ProdutoService;

@Service
@Transactional
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Override
	public Produto criarProduto(Produto produto) {
		return produtoRepository.save(produto);
	}

	@Override
	public Page<Produto> listarProdutos(Integer page, Integer perPage) {
		PageRequest pageRequest = PageRequest.of(page, perPage);
		Page<Produto> queryPage = produtoRepository.findAll(pageRequest);

		return queryPage;
	}

	@Override
	public Produto atualizarProduto(UUID id, Produto produto) {
		Produto produtoOriginal = produtoRepository.findById(id)
				.orElseThrow(() -> new ProdutoNaoEncontradoException("Produto não encontrado."));

		produtoOriginal.setAtivo(produto.getAtivo());
		produtoOriginal.setNome(produto.getNome());
		// produto não pode ter seu tipo alterado, pois isso pode quebrar a regra para
		// desconto em pedidos
		produtoOriginal.setValor(produto.getValor());

		return produtoRepository.save(produtoOriginal);
	}

	@Override
	public Produto obterProduto(UUID id) {
		Produto produto = produtoRepository.findById(id)
				.orElseThrow(() -> new ProdutoNaoEncontradoException("Produto não encontrado."));

		return produto;
	}

	@Override
	public void excluirProduto(UUID id) {
		Produto produto = produtoRepository.findById(id)
				.orElseThrow(() -> new ProdutoNaoEncontradoException("Produto não encontrado."));

		if (produto.getItensPedido().size() == 0) {
			produtoRepository.delete(produto);
		} else {
			throw new ProdutoEmPedidoException();
		}
	}
}
