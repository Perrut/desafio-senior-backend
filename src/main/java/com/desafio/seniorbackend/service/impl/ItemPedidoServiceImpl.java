package com.desafio.seniorbackend.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.seniorbackend.domain.entity.ItemPedido;
import com.desafio.seniorbackend.domain.entity.Pedido;
import com.desafio.seniorbackend.domain.enums.TipoProduto;
import com.desafio.seniorbackend.domain.repository.ItemPedidoRepository;
import com.desafio.seniorbackend.domain.repository.PedidoRepository;
import com.desafio.seniorbackend.domain.repository.ProdutoRepository;
import com.desafio.seniorbackend.exception.ItemPedidoNaoEncontradoException;
import com.desafio.seniorbackend.exception.PedidoNaoEncontradoException;
import com.desafio.seniorbackend.exception.ProdutoInativoException;
import com.desafio.seniorbackend.exception.ProdutoNaoEncontradoException;
import com.desafio.seniorbackend.service.ItemPedidoService;
import com.desafio.seniorbackend.service.PedidoService;

@Service
@Transactional
public class ItemPedidoServiceImpl implements ItemPedidoService {

	@Autowired
	private ItemPedidoRepository itemPedidoRepository;

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private PedidoService pedidoService;

	@Override
	public ItemPedido criarItemPedido(UUID pedidoId, @Valid ItemPedido itemPedido) {
		itemPedido.setPedido(
				pedidoRepository.findById(pedidoId).orElseThrow(() -> new PedidoNaoEncontradoException()));
		itemPedido.setProduto(produtoRepository.findById(itemPedido.getProduto().getId())
				.orElseThrow(() -> new ProdutoNaoEncontradoException("Produto não encontrado.")));

		if (!itemPedido.getProduto().getAtivo()) {
			throw new ProdutoInativoException("Produto está inativo, não pode ser associado à pedido.");
		}

		return itemPedidoComTotalCalculado(itemPedidoRepository.save(itemPedido));
	}

	@Override
	public Page<ItemPedido> listarItensPedido(UUID pedidoId, Integer page, Integer perPage) {
		Pedido pedido = pedidoRepository.findById(pedidoId)
				.orElseThrow(() -> new PedidoNaoEncontradoException());
		
		PageRequest pageRequest = PageRequest.of(page, perPage);
		Page<ItemPedido> queryPage = itemPedidoRepository.buscarPorPedido(pedido, pageRequest);
		Page<ItemPedido> queryFormatada = queryPage.map(itemPedido -> itemPedidoComTotalCalculado(itemPedido));

		return queryFormatada;
	}

	@Override
	public ItemPedido atualizarItemPedido(UUID pedidoId, UUID id, @Valid ItemPedido itemPedido) {
		ItemPedido itemPedidoOriginal = obterItemPedido(pedidoId, id);

		itemPedidoOriginal.setProduto(produtoRepository.findById(itemPedido.getProduto().getId())
				.orElseThrow(() -> new ProdutoNaoEncontradoException("Produto não encontrado.")));
		itemPedidoOriginal.setQuantidade(itemPedido.getQuantidade());

		return itemPedidoComTotalCalculado(itemPedidoRepository.save(itemPedidoOriginal));
	}

	@Override
	public ItemPedido obterItemPedido(UUID pedidoId, UUID id) {
		return Optional.ofNullable(itemPedidoRepository.buscarPorPedidoIdEItemPedidoId(pedidoId, id))
				.orElseThrow(() -> new ItemPedidoNaoEncontradoException());
	}

	@Override
	public void excluirItemPedido(UUID pedidoId, UUID id) {
		ItemPedido itemPedido = obterItemPedido(pedidoId, id);
		Pedido pedido = pedidoRepository.findById(pedidoId).orElseThrow(() -> new PedidoNaoEncontradoException());

		itemPedidoRepository.delete(itemPedido);
		
		List<ItemPedido> produtosTipoProduto = pedido.getItensPedido().stream()
				.filter(item -> item.getProduto().getTipoProduto().equals(TipoProduto.PRODUTO)
						&& !item.equals(itemPedido))
				.collect(Collectors.toList());
		
		if(produtosTipoProduto.size() == 0) {
			pedido.setDesconto(0.0);
		}
		
		pedidoRepository.save(pedido);
		pedidoService.atualizarPedido(pedidoId, pedido);
	}

	@Override
	public ItemPedido itemPedidoComTotalCalculado(ItemPedido item) {
		item.setTotal(item.getProduto().getValor() * item.getQuantidade());

		return item;
	}
}
