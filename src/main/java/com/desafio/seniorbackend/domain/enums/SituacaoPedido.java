package com.desafio.seniorbackend.domain.enums;

public enum SituacaoPedido {
	ABERTO, FECHADO
}
