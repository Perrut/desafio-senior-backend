package com.desafio.seniorbackend.domain.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desafio.seniorbackend.domain.entity.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, UUID> {
}