package com.desafio.seniorbackend.domain.repository;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.desafio.seniorbackend.domain.entity.ItemPedido;
import com.desafio.seniorbackend.domain.entity.Pedido;

@Repository
public interface ItemPedidoRepository extends JpaRepository<ItemPedido, UUID> {

	@Query("select i from ItemPedido i where i.pedido in (select p from Pedido p where id = ?1) and i.id = ?2")
	ItemPedido buscarPorPedidoIdEItemPedidoId(UUID pedidoId, UUID id);

	@Query("select i from ItemPedido i where i.pedido = :pedido")
	Page<ItemPedido> buscarPorPedido(@Param("pedido") Pedido pedido, Pageable pageRequest);
}

