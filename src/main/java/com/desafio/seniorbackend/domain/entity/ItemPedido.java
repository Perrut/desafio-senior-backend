package com.desafio.seniorbackend.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ItemPedido extends AbstractEntity {
	
	@ManyToOne
	@NotNull
    @JoinColumn(name = "produto_id")
	private Produto produto;
	
	@ManyToOne
	@NotNull
	@JsonIgnore
	@JoinColumn(name = "pedido_id")
	private Pedido pedido;

	@Min(value = 1, message = "Um item de pedido deve possuir pelo menos uma unidade do produto.")
	@Column(nullable = false)
	private Integer quantidade;
	
	@Transient
	private Double total;

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
}
