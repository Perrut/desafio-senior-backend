package com.desafio.seniorbackend.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.desafio.seniorbackend.domain.enums.SituacaoPedido;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Pedido extends AbstractEntity {
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private SituacaoPedido situacaoPedido = SituacaoPedido.ABERTO;
	
	@Column(nullable = false)
	private Date dataCriacao = new Date();
	
	@Min(value = 0, message = "Desconto não pode ter porcentagem negativa.")
	@Max(value = 90, message = "Desconto não pode ter valor mais do que 90%.")
	@Column(nullable = false)
	private Double desconto = 0.0;
	
	@JsonIgnore
	@OneToMany(mappedBy = "pedido", fetch = FetchType.LAZY)
	private List<ItemPedido> itensPedido = new ArrayList<>();
	
	@Transient
	private Double valorTotal;
	
	@Transient
	private Double valorComDesconto;

	public SituacaoPedido getSituacaoPedido() {
		return situacaoPedido;
	}

	public void setSituacaoPedido(SituacaoPedido situacaoPedido) {
		this.situacaoPedido = situacaoPedido;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<ItemPedido> getItensPedido() {
		return itensPedido;
	}

	public void setItensPedido(List<ItemPedido> itensPedido) {
		this.itensPedido = itensPedido;
	}

	public Double getValorComDesconto() {
		return valorComDesconto;
	}

	public void setValorComDesconto(Double valorComDesconto) {
		this.valorComDesconto = valorComDesconto;
	}
}
