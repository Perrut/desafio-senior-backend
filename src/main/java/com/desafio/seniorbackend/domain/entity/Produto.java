package com.desafio.seniorbackend.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.desafio.seniorbackend.domain.enums.TipoProduto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Produto extends AbstractEntity {

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private TipoProduto tipoProduto;

	@Min(value = 1, message = "Produto deve custar no mínimo R$ 1,00.")
	@Max(value = 10000, message = "Produto deve custar no máximo R$ 10000,00.")
	@Column(nullable = false)
	private Double valor;

	@Column(nullable = false)
	private Boolean ativo = true;

	@NotEmpty(message = "Produto deve possuir um nome.")
	@Column(nullable = false, unique = true)
	private String nome;

	@JsonIgnore
	@OneToMany(mappedBy = "produto", fetch = FetchType.LAZY)
	private List<ItemPedido> itensPedido = new ArrayList<>();

	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<ItemPedido> getItensPedido() {
		return itensPedido;
	}

	public void setItensPedido(List<ItemPedido> itensPedido) {
		this.itensPedido = itensPedido;
	}
}
