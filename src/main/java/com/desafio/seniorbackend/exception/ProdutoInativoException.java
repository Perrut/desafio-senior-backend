package com.desafio.seniorbackend.exception;

public class ProdutoInativoException extends RuntimeException {

	private static final long serialVersionUID = 6888181149785032622L;

	public ProdutoInativoException(String message) {
		super(message);
	}
}
