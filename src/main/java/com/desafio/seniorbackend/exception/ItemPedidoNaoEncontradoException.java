package com.desafio.seniorbackend.exception;

public class ItemPedidoNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = -6359911650096539821L;

	public ItemPedidoNaoEncontradoException() {
		super();
	}
}
