package com.desafio.seniorbackend.exception;

public class PedidoNaoEncontradoException extends RuntimeException {
	
	private static final long serialVersionUID = -5539020443228775622L;

	public PedidoNaoEncontradoException() {
		super();
	}
}
