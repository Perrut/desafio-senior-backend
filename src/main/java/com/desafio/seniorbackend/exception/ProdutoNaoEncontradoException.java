package com.desafio.seniorbackend.exception;

public class ProdutoNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = -8602157446354350150L;

	public ProdutoNaoEncontradoException(String message) {
		super(message);
	}
}
