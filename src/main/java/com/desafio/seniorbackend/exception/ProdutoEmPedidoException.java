package com.desafio.seniorbackend.exception;

public class ProdutoEmPedidoException extends RuntimeException {

	private static final long serialVersionUID = -3831766591276994382L;

	public ProdutoEmPedidoException() {
		super();
	}
}
