package com.desafio.seniorbackend.exception;

public class PedidoFechadoException extends RuntimeException {

	private static final long serialVersionUID = 4012541404510727858L;

	public PedidoFechadoException(String message) {
		super(message);
	}
}
