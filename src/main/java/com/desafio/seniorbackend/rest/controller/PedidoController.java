package com.desafio.seniorbackend.rest.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.seniorbackend.domain.entity.Pedido;
import com.desafio.seniorbackend.service.PedidoService;

@RestController
public class PedidoController {

	@Autowired
	private PedidoService pedidoService;

	@RequestMapping(value = "/pedidos/{page}/{perPage}", method = RequestMethod.GET)
	public Page<Pedido> listarPedidos(@PathVariable(value = "page") Integer page,
			@PathVariable(value = "perPage") Integer perPage) {
		return pedidoService.listarPedidos(page, perPage);
	}

	@RequestMapping(value = "/pedidos", method = RequestMethod.POST)
	public Pedido criarPedido(@Valid @RequestBody Pedido pedido) {
		return pedidoService.criarPedido(pedido);
	}

	@RequestMapping(value = "/pedidos/{id}", method = RequestMethod.PUT)
	public Pedido atualizarPedido(@PathVariable(value = "id") UUID id, @Valid @RequestBody Pedido pedido) {
		return pedidoService.atualizarPedido(id, pedido);
	}

	@RequestMapping(value = "/pedidos/{id}", method = RequestMethod.GET)
	public Pedido obterPedido(@PathVariable(value = "id") UUID id) {
		return pedidoService.obterPedido(id);
	}

	@RequestMapping(value = "/pedidos/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> excluirPedido(@PathVariable(value = "id") UUID id) {
		pedidoService.excluirPedido(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
