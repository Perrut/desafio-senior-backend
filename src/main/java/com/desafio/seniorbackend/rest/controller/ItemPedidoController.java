package com.desafio.seniorbackend.rest.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.seniorbackend.domain.entity.ItemPedido;
import com.desafio.seniorbackend.service.ItemPedidoService;

@RestController
public class ItemPedidoController {

	@Autowired
	private ItemPedidoService itemPedidoService;

	@RequestMapping(value = "/pedidos/{id}/itens/{page}/{perPage}", method = RequestMethod.GET)
	public Page<ItemPedido> listarItensPedido(@PathVariable(value = "id") UUID id,
			@PathVariable(value = "page") Integer page, @PathVariable(value = "perPage") Integer perPage) {
		return itemPedidoService.listarItensPedido(id, page, perPage);
	}

	@RequestMapping(value = "/pedidos/{id}/itens", method = RequestMethod.POST)
	public ItemPedido criarItemPedido(@PathVariable(value = "id") UUID id, @RequestBody ItemPedido itemPedido) {
		return itemPedidoService.criarItemPedido(id, itemPedido);
	}

	@RequestMapping(value = "/pedidos/{pedidoId}/itens/{id}", method = RequestMethod.PUT)
	public ItemPedido atualizarItemPedido(@PathVariable(value = "pedidoId") UUID pedidoId,
			@PathVariable(value = "id") UUID id, @RequestBody ItemPedido itemPedido) {
		return itemPedidoService.atualizarItemPedido(pedidoId, id, itemPedido);
	}

	@RequestMapping(value = "/pedidos/{pedidoId}/itens/{id}", method = RequestMethod.GET)
	public ItemPedido obterItemPedido(@PathVariable(value = "pedidoId") UUID pedidoId,
			@PathVariable(value = "id") UUID id) {
		return itemPedidoService.obterItemPedido(pedidoId, id);
	}

	@RequestMapping(value = "/pedidos/{pedidoId}/itens/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> excluirPedido(@PathVariable(value = "pedidoId") UUID pedidoId,
			@PathVariable(value = "id") UUID id) {
		itemPedidoService.excluirItemPedido(pedidoId, id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
