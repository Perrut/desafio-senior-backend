package com.desafio.seniorbackend.rest.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.seniorbackend.domain.entity.Produto;
import com.desafio.seniorbackend.service.ProdutoService;

@RestController
public class ProdutoController {
	@Autowired
	private ProdutoService produtoService;

	@RequestMapping(value = "/produtos/{page}/{perPage}", method = RequestMethod.GET)
	public Page<Produto> listarProdutos(@PathVariable(value = "page") Integer page,
			@PathVariable(value = "perPage") Integer perPage) {
		return produtoService.listarProdutos(page, perPage);
	}

	@RequestMapping(value = "/produtos", method = RequestMethod.POST)
	public Produto criarProduto(@Valid @RequestBody Produto produto) {
		return produtoService.criarProduto(produto);
	}

	@RequestMapping(value = "/produtos/{id}", method = RequestMethod.PUT)
	public Produto atualizarProduto(@PathVariable(value = "id") UUID id, @Valid @RequestBody Produto produto) {
		return produtoService.atualizarProduto(id, produto);
	}

	@RequestMapping(value = "/produtos/{id}", method = RequestMethod.GET)
	public Produto obterProduto(@PathVariable(value = "id") UUID id) {
		return produtoService.obterProduto(id);
	}

	@RequestMapping(value = "/produtos/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> excluirProduto(@PathVariable(value = "id") UUID id) {
		produtoService.excluirProduto(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
