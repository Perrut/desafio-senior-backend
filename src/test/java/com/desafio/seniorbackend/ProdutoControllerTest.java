package com.desafio.seniorbackend;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.desafio.seniorbackend.domain.entity.Produto;
import com.desafio.seniorbackend.domain.enums.TipoProduto;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProdutoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void deveCriarUmProduto() throws Exception {
		Produto produto = new Produto();
		produto.setAtivo(true);
		produto.setNome("Shampoo");
		produto.setTipoProduto(TipoProduto.SERVICO);
		produto.setValor(20.0);

		this.mockMvc.perform(post("/produtos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(produto)))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("Shampoo")));
	}

	@Test
	public void deveAtualizarUmProduto() throws Exception {
		Produto produto = new Produto();
		produto.setAtivo(true);
		produto.setNome("Shampoo");
		produto.setTipoProduto(TipoProduto.SERVICO);
		produto.setValor(20.0);

		ResultActions res = this.mockMvc
				.perform(post("/produtos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(produto)));

		Produto produtoAtualizado = fromJsonResult(res.andReturn(), Produto.class);
		produtoAtualizado.setNome("Sabonete");

		this.mockMvc
				.perform(put("/produtos/" + produtoAtualizado.getId().toString())
						.contentType(MediaType.APPLICATION_JSON).content(asJsonString(produtoAtualizado)))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("Sabonete")));
	}

	@Test
	public void deveExcluirUmProduto() throws Exception {
		Produto produto = new Produto();
		produto.setAtivo(true);
		produto.setNome("Shampoo");
		produto.setTipoProduto(TipoProduto.SERVICO);
		produto.setValor(20.0);

		ResultActions res = this.mockMvc
				.perform(post("/produtos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(produto)));

		Produto produtoExclusao = fromJsonResult(res.andReturn(), Produto.class);

		this.mockMvc.perform(
				delete("/produtos/" + produtoExclusao.getId().toString()).contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isNoContent());
	}

	@Test
	public void deveObterProdutoPorId() throws Exception {
		Produto produto = new Produto();
		produto.setAtivo(true);
		produto.setNome("Shampoo");
		produto.setTipoProduto(TipoProduto.SERVICO);
		produto.setValor(20.0);

		ResultActions res = this.mockMvc
				.perform(post("/produtos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(produto)));

		Produto produtoGet = fromJsonResult(res.andReturn(), Produto.class);

		this.mockMvc.perform(get("/produtos/" + produtoGet.getId().toString()).contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString(produtoGet.getId().toString())));
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public <T> T fromJsonResult(MvcResult result, Class<T> tClass) throws Exception {
		return this.mapper.readValue(result.getResponse().getContentAsString(), tClass);
	}
}
