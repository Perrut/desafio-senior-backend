package com.desafio.seniorbackend;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.desafio.seniorbackend.domain.entity.Pedido;
import com.desafio.seniorbackend.domain.enums.SituacaoPedido;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class PedidoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void deveCriarUmPedido() throws Exception {
		Pedido pedido = new Pedido();
		pedido.setDataCriacao(new Date());

		this.mockMvc.perform(post("/pedidos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(pedido)))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("0.0")))
				.andExpect(content().string(containsString("ABERTO")));
	}

	@Test
	public void deveAtualizarUmPedido() throws Exception {
		Pedido pedido = new Pedido();
		pedido.setDataCriacao(new Date());

		ResultActions res = this.mockMvc
				.perform(post("/pedidos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(pedido)));

		Pedido pedidoAtualizado = fromJsonResult(res.andReturn(), Pedido.class);
		pedidoAtualizado.setSituacaoPedido(SituacaoPedido.FECHADO);

		this.mockMvc
				.perform(put("/pedidos/" + pedidoAtualizado.getId().toString()).contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(pedidoAtualizado)))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("FECHADO")));
	}

	@Test
	public void deveExcluirUmPedido() throws Exception {
		Pedido pedido = new Pedido();
		pedido.setDataCriacao(new Date());

		ResultActions res = this.mockMvc
				.perform(post("/pedidos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(pedido)));

		Pedido pedidoExclusao = fromJsonResult(res.andReturn(), Pedido.class);

		this.mockMvc
				.perform(
						delete("/pedidos/" + pedidoExclusao.getId().toString()).contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isNoContent());
	}

	@Test
	public void deveObterPedidoPorId() throws Exception {
		Pedido pedido = new Pedido();
		pedido.setDataCriacao(new Date());

		ResultActions res = this.mockMvc
				.perform(post("/pedidos").contentType(MediaType.APPLICATION_JSON).content(asJsonString(pedido)));

		Pedido pedidoGet = fromJsonResult(res.andReturn(), Pedido.class);

		this.mockMvc.perform(get("/pedidos/" + pedidoGet.getId().toString()).contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString(pedidoGet.getId().toString())));
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public <T> T fromJsonResult(MvcResult result, Class<T> tClass) throws Exception {
		return this.mapper.readValue(result.getResponse().getContentAsString(), tClass);
	}
}
