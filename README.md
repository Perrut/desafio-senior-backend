# Desafio Senior - Backend

Este desafio foi elaborado com base na prova Nível 3

## Requisitos para a execução

- Java 11
- Postgresql
- Docker (mas é possível executar sem o Docker também)

## Execução

- Todos os comandos abaixo são executados na pasta do projeto onde se econtra o arquivo pom.xml (esta pasta)

- Para executar o projeto é necessário primeiramente compilar com o comando:
```
./mvnw clean install
```
- O comando acima irá executar com os testes, caso queira executar sem os mesmos basta rodar:
```
./mvnw clean install -Dmaven.test.skip=true
```
- Feito isto, basta executar o comando
```
docker-compose up
```
- O projeto estará disponível para execução no endereço http://localhost:8080
- Se houver algum problema relacionado à não existência da base de dados, pode ser porque o usuário já possui uma imagem docker do Postgres rodando na sua máquina com um volume de mesmo nome, neste caso, basta apagar o volume com o comando
```
docker volume rm nome_do_volume
```
- É possível também comentar o serviço app no docker-compose e iniciar apenas a base de dados, nesse cenário, deve se utilizar o ip da máquina no arquivo application.properties da pastar src/resources
- Também é possível criar a base com nome seniorbackend numa instlação local do Postgres tendo apenas que fazer o mesmo passo acima no arquivo application.properties para se executar o projeto

## Modelos

### Produto

- O modelo de json da classe Produto para criação é o seguinte:
```
{
    "tipoProduto": "PRODUTO", // pode ser PRODUTO ou SERVICO
    "valor": 500,
    "nome": "teste-produto",
    "ativo": true
}
```

### Pedido

- O modelo de json da classe Pedido para criação é o seguinte:
```
{
    "dataCriacao": "2020-12-05 17:30"
}
```

### ItemPedido

- O modelo de json da classe ItemPedido para criação é o seguinte:
```
{
    "produto": {
        "id": "2ca44bb4-d215-496f-8a62-53c4c6c83d53",
        "tipoProduto": "SERVICO",
        "valor": 500.0,
        "ativo": false,
        "nome": "teste3"
    },
    "quantidade": 1
}
```

## Endpoints

### Produto

- `POST` http://localhost:8080/produtos
- `PUT` http://localhost:8080/produtos/${idProduto}
- `GET` http://localhost:8080/produtos/${idProduto}
- `DELETE` http://localhost:8080/produtos/${idProduto}
- `GET` http://localhost:8080/produtos/0/5 (com paginação, primeiro número é o índice da página, segundo número é a quantidade de itens por página (variáveis))

### Pedido

- `POST` http://localhost:8080/pedidos
- `PUT` http://localhost:8080/pedidos/${idPedido}
- `GET` http://localhost:8080/pedidos/${idPedido}
- `DELETE` http://localhost:8080/pedidos/${idPedido}
- `GET` http://localhost:8080/pedidos/0/5 (com paginação, primeiro número é o índice da página, segundo número é a quantidade de itens por página (variáveis))

### ItemPedido

- `POST` http://localhost:8080/pedidos/${idPedido}/itens
- `PUT` http://localhost:8080/pedidos/${idPedido}/itens/{idItemPedido}
- `GET` http://localhost:8080/pedidos/${idPedido}/itens/{idItemPedido}
- `DELETE` http://localhost:8080/pedidos/${idPedido}/itens/{idItemPedido}
- `GET` http://localhost:8080/pedidos/${idPedido}/itens/0/5 (com paginação, primeiro número é o índice da página, segundo número é a quantidade de itens por página (variáveis))

## Testes

- Para testar o projeto, basta executar o comando:
```
./mvnw clean compile test
```

## Collection

Há um arquivo no projeto: `Desafio Senior.postman_collection.json` com uma collection do Postman para facilitar a execução da API.

## Considerações

- Na branch `feat/tentativa-query-dsl`, há classes de uma tentativa de implementação de query DSL (único requisito da prova em que não consegui fazer, pois estava com pouco tempo e se focasse em resolver o bug que tomei não conseguiria entregar o desafio), porém, pode se perceber que parei apenas em algo que se eu tivesse mais tempo de depurar conseguiria fazer.

- Foi meu primeiro projeto com Spring Boot, gostei bastante.

- Com relação aos testes também não fiz muitos por conta do tempo, mas fiz o suficiente para mostrar que entendo a técnica.

- Gostei muito de fazer esse desafio, qualquer que seja o resultado agradeço muito a oportunidade.
